var neo4j = require('neo4j');
var db = new neo4j.GraphDatabase('http://neo4j:katie@localhost:7474');

module.exports = {

    addNewUser: function(userName, password) {
        db.cypher({
            query: ' MATCH (u:User) WITH MAX(u.userId)+1 AS max CREATE (n:User { userId: max, userName: {userName}, password:{password} })',
                params: {
                    userName: userName,
                    password: password
                },
        }, callback);

        function callback(err, results) {
            if (err) throw err;
            var result = results[0];
            if (!result) {
                console.log('No user found.');
            } else {
                var user = result.userId;
                console.log(user);
            }
        }
    },

    getUser: (function(userName, password) {
        var value;
        db.cypher({
            query: 'MATCH (u:User { userName: {userName}, password:{password} }) return u.userId as userId;',
                params: {
                    userName: userName,
                    password: password
                },
        }).then(function (err, results) {
            if (err) console.log(err);
            var result = results[0];      
            if (!result) {
                console.log('No user found.');
            } else {
                value = result.userId;
            }
            console.log(value);
        });

        }).then(function() {
            return value;
        })
    };