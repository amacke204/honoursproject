//requires'
var neo4j = require('neo4j');
var $ = require('jquery');
var imdb = require('imdb-api');
//globals
var db = new neo4j.GraphDatabase('http://neo4j:katie@localhost:7474');
var loggedInUserId = 23;
var movieDetailsId = 0;

//on ready
$( document ).ready(function(){
    init();
    checkLogin();
    $('#detailsView').hide();
});

//initialises listeners
function init() {
    //find accordion
    $('#accordion').find('.accordion-toggle').click(function(e){
        e.preventDefault();
        //Expand or collapse this panel
        $(this).next().slideToggle('fast');

        //Hide the other panels
        $(".accordion-content").not($(this).next()).slideUp('fast');
        var section = $(this);
        //switch based on section to dynamically populate
        switch(section.attr('id')) {

            case "genreSection":
                startLoader(section);
                getGenreSectionData(section);
                checkLogin();
                break;

            case "contentBasedSection":
                startLoader(section);
                getContentBasedSectionData(section);
                checkLogin();
                break;

            case "collaborativeBasedSection":
                startLoader(section);
                getCollaborativeBasedSectionData(section);
                checkLogin();
                break;

            case "hybridBasedSection":
                startLoader(section);
                getHybridBasedSectionData(section);
                checkLogin();
                break;

            default:
                throw new Error('Unknown id: ' + section.attr('id'));
        }

    });

    //rudimentary login functionality 
    //to allow for user identification during interaction
    $("#logIn").on( "click", function(e) {
        e.preventDefault();
        var userName = $('#userName').val();
        var password = $('#password').val();
        console.time("logIn");
        db.cypher({
            query: 'MATCH (u:User { userName: {userName}, password:{password} }) RETURN u.userId as userId;',
                params: {
                    userName: userName,
                    password: password
                },
        }, 
        function (err, results) {
            if (err) console.log(err);
            console.timeEnd("logIn");
            var result = results[0];
            loggedInUserId = result.userId;
            checkLogin();
        });
    });

    $("#logOut").on( "click", function(e) {
        e.preventDefault();
        loggedInUserId = 0;
        $('#userName').val(null);
        $('#password').val(null);
        checkLogin();
    });
    
    $("#createNew").on( "click", function(e) {
        e.preventDefault();
        console.time("createNew");
        var userName = $('#userName').val();
        var password = $('#password').val();
        db.cypher({
            query:  ' MATCH (u:User) WITH MAX(u.userId)+1 AS max' + 
                    ' CREATE (n:User { userId: max, userName: {userName}, password:{password} })' +
                    ' RETURN n.userId as userId',
                params: {
                    userName: userName,
                    password: password
                },
        }, 
        function (err, results) {
            if (err) console.log(err);
            console.timeEnd("createNew");
            var result = results[0];
            loggedInUserId = result.userId;
            checkLogin();
            setSimilarity();
        });
    });

    $('#detailsView').on( "click", ".ratingLabel", function(e) {
        e.preventDefault();
        var ratingId = this.id.toString().slice(-1);
        $("#star" + ratingId ).prop("checked", true);
        setRating(ratingId);
    });
}

//set rating
function setRating(rating) {
    console.time("setRating");
    db.cypher({
    query:      'MATCH (m:Movie { movieId: {movieId} }) ' +
                'MATCH (u:User { userId: {loggedInUserId} }) ' +
                'CREATE (u)-[r:HAS_RATING {rating:  toFloat({rating}) }]->(m);',
            params: {
                movieId: movieDetailsId,
                loggedInUserId: loggedInUserId,
                rating: rating
            },
    }, 
    function (err, results) {
        if (err) console.log(err);
        console.timeEnd("setRating");
    });
}





function setSimilarity() {
    console.time("setSimilarity");
    db.cypher({
    query:      'MATCH (u1:User {userId: {loggedInUserId}})-[x:HAS_RATING]-> (m:Movie) <-[y:HAS_RATING]-(u2:User) ' +
                'WITH count (m) AS commonMovies, u1.userId AS user1, u2.userId AS user2, u1, u2, ' +
                'collect((x.rating-y.rating)^2) AS ratings, ' +
                'collect(m.movieId) AS movies ' +
                'WITH commonMovies, movies, u1, u2, ratings ' +
                'MERGE (u1)-[s:HAS_SIMILARITY]->(u2)  ' +
                'SET s.similarity = 1-(SQRT(reduce(total=0.0, k in extract(i in ratings | i/commonMovies) | total+k))/4);',
            params: {
                loggedInUserId: loggedInUserId,
            },
    }, 
    function (err, results) {
        if (err) console.log(err);
        console.timeEnd("setSimilarity");
    });
}

//get rating
function getRating() {
    console.time("getRating");
    db.cypher({
    query:      'MATCH (m:Movie { movieId: {movieId} }) ' +
                'MATCH (u:User { userId: {loggedInUserId} }) ' +
                'MATCH (u)-[r:HAS_RATING] ->(m) ' +
                'RETURN r.rating AS r;',
            params: {
                movieId: movieDetailsId,
                loggedInUserId: loggedInUserId,
            },
    }, 
    function (err, results) {
        if (err) console.log(err);
            console.timeEnd("getRating");
        if (results.length > 0)
            $("#star" + results[0].r ).prop("checked", true);
    });
}

//get data section
function getGenreSectionData(section) {
    //Get graph data
    var genre = section.data("genre");
    console.time("getGenreSectionData");
    db.cypher({
        query:  ' MATCH (m:Movie)-[:HAS_GENRE]->(g:Genre {genre:{genre}})' + 
                ' MATCH ()-[r:HAS_RATING]->(m)' +
                ' WHERE r.rating > 4.5' +
                ' RETURN m AS rec, count(*) AS weight ORDER BY weight DESC LIMIT 20;',
            params: {
                genre: genre
            },
    }, 
    function (err, results) {
        if (err) console.log(err);
        console.timeEnd("getGenreSectionData");
        populateSection(section, results);
    });
}

function getContentBasedSectionData(section) {
    //Get graph data
    console.time("getContentBasedSectionData");
    db.cypher({
        query:  'MATCH (user:User {userId: {loggedInUserId} })-[r:HAS_RATING]->(movie:Movie), ' +
                '      (movie)-[:HAS_GENRE]->(genre:Genre), ' +
                '      (genre)<-[:HAS_GENRE]-(rec:Movie) ' +
                'WHERE r.rating > 4 ' +
                'AND NOT (user)-[:HAS_RATING]->(rec) ' +
                'RETURN rec, (count(*)/10) AS weight ORDER BY weight DESC LIMIT 20;',
            params: {
                loggedInUserId: loggedInUserId
            },
    }, 
    function (err, results) {
        if (err) console.log(err);
        console.timeEnd("getContentBasedSectionData");
        populateSection(section, results);
    });
}

function getCollaborativeBasedSectionData(section) {
    //Get graph 
    console.time("getCollaborativeBasedSectionData");
    db.cypher({
        query:  'MATCH (u1:User)-[r:HAS_RATING]->(m:Movie), ' +
                '      (u1)<-[s:HAS_SIMILARITY]-(u2:User { userId: {loggedInUserId} }) ' +
                'WHERE NOT ((u2)-[:HAS_RATING]->(m)) ' +
                'WITH m, r.rating AS rating, s.similarity as similarity ' +
                'WHERE similarity > 0.6 ' +
                'WITH m, COLLECT(similarity) AS similarities, ' + 
                'COLLECT(rating) AS ratings ' +
                'WITH REDUCE(x = 0, i IN similarities | x+i)*1.0 /  ' +
                'LENGTH(similarities) AS avgSimilarity, m, similarities,  ' +
                'REDUCE(x = 0, i IN ratings | x+i)*1.0 /  ' +
                'LENGTH(ratings) AS avgRating, ratings ' +
                'ORDER BY LENGTH(ratings) DESC, avgRating DESC, avgSimilarity DESC ' +
                'WHERE avgRating >4 ' +
                'RETURN m AS rec, LENGTH(ratings) as weight LIMIT 20;',
            params: {
                loggedInUserId: loggedInUserId
            },
    }, 
    function (err, results) {
        if (err) console.log(err);
        console.timeEnd("getCollaborativeBasedSectionData");
        populateSection(section, results);
    });
}

function getHybridBasedSectionData(section) {
    var contentResults;
    var collabResults;
    console.time("getHybridBasedSectionData");
    db.cypher({
        query:  'MATCH (user:User {userId: {loggedInUserId} })-[r:HAS_RATING]->(movie:Movie), ' +
                '      (movie)-[:HAS_GENRE]->(genre:Genre), ' +
                '      (genre)<-[:HAS_GENRE]-(rec:Movie) ' +
                'WHERE r.rating > 4 ' +
                'AND NOT (user)-[:HAS_RATING]->(rec) ' +
                'RETURN rec, (count(*)/10) AS weight ORDER BY weight DESC LIMIT 10;',
            params: {
                loggedInUserId: loggedInUserId
        },
    },  
    function (err, results) {
        if (err) console.log(err);
        contentResults = results;
        db.cypher({
            query:  'MATCH (u1:User)-[r:HAS_RATING]->(m:Movie), ' +
                    '    (u1)<-[s:HAS_SIMILARITY]-(u2:User { userId: {loggedInUserId} }) ' +
                    'WHERE NOT ((u2)-[:HAS_RATING]->(m)) ' +
                    'WITH m, r.rating AS rating, s.similarity as similarity ' +
                    'WHERE similarity > 0.6 ' +
                    'WITH m, COLLECT(similarity) AS similarities, ' + 
                    'COLLECT(rating) AS ratings ' +
                    'WITH REDUCE(x = 0, i IN similarities | x+i)*1.0 /  ' +
                    'LENGTH(similarities) AS avgSimilarity, m, similarities,  ' +
                    'REDUCE(x = 0, i IN ratings | x+i)*1.0 /  ' +
                    'LENGTH(ratings) AS avgRating, ratings ' +
                    'ORDER BY LENGTH(ratings) DESC, avgRating DESC, avgSimilarity DESC ' +
                    'WHERE avgRating >4 ' +
                    'RETURN m AS rec, LENGTH(ratings) as weight LIMIT 10; ',
                params: {
                    loggedInUserId: loggedInUserId
                },
        },  
        function (err, results) {
            if (err) console.log(err);
            collabResults = results;
            var combinedArray = $.merge(contentResults, collabResults);
            combinedArray.sort(function (a,b) {
                if(a.weight < b.weight){ return 1;}
                    if(a.weight > b.weight){ return -1;}
                        return 0;
            });
            console.timeEnd("getHybridBasedSectionData");
            populateSection(section, combinedArray);
        });     
    });
}

//starts loading spinner
function startLoader(section) {
    //document.getElementById("toggle_div").style.display="block";
    var populationSection = section.next();
    var loader = document.createElement("div");
    loader.className = "loader";
    populationSection.append(loader);
}

//removes loading spinner
function endLoader() {
    //document.getElementById("toggle_div").style.display="none";
    $(".loader").remove();
}

//population method used by each accordion section
//after getting required data
function populateSection(section, results) {
    endLoader();
    var populationSection = section.next();
    $( "#" + populationSection[0].id ).empty();
    //iterate then create movie divs
    $.each(results , function(i, val) { 
        var imdbId = val.rec.properties.imdbId;
        var movieDiv = document.createElement("div");
        movieDiv.className = "movieDiv col-md-2";
        movieDiv.id = imdbId;
        movieDiv.onclick = function() { getMovieDetails(imdbId, val.rec.properties.movieId); startLoader($('#detailsView')); };
        var movieTitle = document.createElement("p");
        movieTitle.className = "name";
        movieTitle.innerHTML = val.rec.properties.title;
        movieDiv.append(movieTitle);
        populationSection.append(movieDiv);
    });

    $("html, body").animate({ scrollTop: $("#" + populationSection[0].id).offset().top }, 500);
}

//get details about specific movie via imdb api package
function getMovieDetails(imdbId, movieId) {
    imdbId.toString();
    //pad zeros for allowed id length of 9 chars
    while (imdbId.length < 7) {
        imdbId = '0' + imdbId;
    }
    imdb.getById('tt' + imdbId , function (err, movie) {
        if (err) console.log(err);
        populateMovieDetails(movie, movieId);
    });
}

function populateMovieDetails(movie, movieId) {
    movieDetailsId = movieId;
    endLoader();
    $('#detailsView').empty();
    var movieDetailsDiv = document.createElement("div");
    movieDetailsDiv.className = "movieDetailsDiv col-md-12";
    
    //Movie Title
    var movieTitleDiv = document.createElement("div");
    movieTitleDiv.className = "movieTitleDiv col-md-8";
    var movieTitle = document.createElement("h2");
    movieTitle.className = "detailsName";
    movieTitle.innerHTML = "Title: " + movie.title;
    movieTitleDiv.append(movieTitle);
    movieDetailsDiv.append(movieTitleDiv);

    //Year of Release
    var movieYearDiv = document.createElement("div");
    movieYearDiv.className = "movieYearDiv col-md-4";
    var movieYear = document.createElement("h2");
    movieYear.className = "detailsName";
    movieYear.innerHTML = "Year Of Release: " + movie._year_data;
    movieYearDiv.append(movieYear);
    movieDetailsDiv.append(movieYearDiv);

    //Poster
    var moviePosterDiv = document.createElement("div");
    moviePosterDiv.className = "moviePosterDiv col-md-6";
    var moviePosterImg = document.createElement("img");
    moviePosterImg.className = "detailsImage";
    moviePosterImg.src = movie.poster;
    moviePosterImg.alt = movie.title;
    moviePosterImg.title = movie.title;
    moviePosterDiv.append(moviePosterImg);
    movieDetailsDiv.append(moviePosterDiv);

    //Plot
    var moviePlotDiv = document.createElement("div");
    moviePlotDiv.className = "moviePlotDiv col-md-6";
    var moviePlotDivTitle = document.createElement("h4");
    moviePlotDivTitle.className = "plotTitle";
    moviePlotDivTitle.innerHTML = "Plot synopsis: ";
    var moviePlot = document.createElement("p");
    moviePlot.className = "detailsDescription";
    moviePlot.innerHTML = movie.plot;
    moviePlotDiv.append(moviePlotDivTitle);
    moviePlotDiv.append(moviePlot);
    movieDetailsDiv.append(moviePlotDiv);

    //Rating 
    var movieRatingContainer = document.createElement("div");
    movieRatingContainer.className = "col-md-6";
    var movieRatingFieldset = document.createElement("fieldset");
    movieRatingFieldset.className = "rating";
    var movieRatingLegend = document.createElement("legend");
    movieRatingLegend.innerHTML = "Rating: ";
    movieRatingFieldset.append(movieRatingLegend);

    for (var i = 5; i > 0; i--)
    {
        var movieRatingInput = document.createElement("input")
        movieRatingInput.type = "radio";
        movieRatingInput.id = "star" + (i);
        movieRatingInput.name = "rating";
        movieRatingInput.value = i;
        var movieRatingInputLabel = document.createElement("label");
        movieRatingInputLabel.for = "star" + (i);
        movieRatingInputLabel.title = (i) + " Stars";
        movieRatingInputLabel.id = "starlabel" + (i);
        movieRatingInputLabel.className = "ratingLabel";
        movieRatingFieldset.append(movieRatingInput);
        movieRatingFieldset.append(movieRatingInputLabel);
    }

    movieRatingContainer.append(movieRatingFieldset);
    movieDetailsDiv.append(movieRatingContainer);

    $('#detailsView').append(movieDetailsDiv);
    getRating();
    checkLogin();
    $('#detailsView').show();
    $("html, body").animate({ scrollTop: $("#detailsView").offset().top }, 500);

}

//hit this after user interaction to verify views.    
function checkLogin() {
    if( loggedInUserId === 0 ) {
        $('#logInContainer').show();
        $('#loggedInContainer').hide();
        $('#collaborativeBasedSection').hide();
        $('#contentBasedSection').hide();
        $('#hybridBasedSection').hide();
        $('.rating').hide();
    } else {
        $('#logInContainer').hide();
        $('#loggedInContainer').show();
        $('#collaborativeBasedSection').show();
        $('#contentBasedSection').show();
        $('#hybridBasedSection').show();
        $('.rating').show();
    }
}